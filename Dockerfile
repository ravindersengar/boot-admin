FROM openjdk:8-jdk-alpine
EXPOSE 4003
VOLUME /main-app
ADD /target/boot-admin-0.1.0.jar boot-admin-0.1.0.jar
ENTRYPOINT ["java","-jar","boot-admin-0.1.0.jar"]